﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TileEvent
{
    public abstract void OnMatch();
    public abstract bool AchievementCompleted();
}

public class CakeTileEvent : TileEvent
{

    int cakeAmount;

    public CakeTileEvent(int _cakeAmount = 3)
    {
        this.cakeAmount = _cakeAmount;
    }

    public override void OnMatch()
    {
        Debug.Log("Selinting cake ditangan " + cakeAmount);
        cakeAmount -= 1;
    }

    public override bool AchievementCompleted()
    {
        return cakeAmount < 1;
    }
}

public class CookiesTileEvent : TileEvent
{
    int cookiesAmount;

    public CookiesTileEvent(int _cookiesAmount = 3)
    {
        this.cookiesAmount = _cookiesAmount;
    }

    public override void OnMatch()
    {
        Debug.Log("Selinting cookies ditangan " + cookiesAmount);
        cookiesAmount -= 1;
    }

    public override bool AchievementCompleted()
    {
        return cookiesAmount < 1;
    }
}

public class GumTileEvent : TileEvent
{
    int gumAmount;

    public GumTileEvent(int _gumAmount = 3)
    {
        this.gumAmount = _gumAmount;
    }

    public override void OnMatch()
    {
        Debug.Log("Selinting Gum ditangan " + gumAmount);
        gumAmount -= 1;
    }

    public override bool AchievementCompleted()
    {
        return gumAmount < 1;
    }
}
