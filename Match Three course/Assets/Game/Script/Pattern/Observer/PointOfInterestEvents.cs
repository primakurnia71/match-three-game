﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PointOfInterestEvents : MonoBehaviour
{

    private void OnEnable()
    {
        AchievementSystemEvents.onMatch += Match;
    }

    public void Match()
    {
        AchievementSystemEvents.onMatch -= Match;
    }
}
